<#

.SYNOPSIS

Bootstrap And Run Now!!! For Windows PowerShell.

.DESCRIPTION

Bootstraps Windows software for one or more profiles.  Profiles consist of a list of modules as defined in the "modules" subdirectory.  Multiple profiles can be installed resulting in the union of all modules in the provided profiles.

This script requires Administrator privileges to run.  UAC will prompt for permissions if necessary.
		
.EXAMPLE

.\barn.ps1 -Profiles "home,experimental"

This bootstraps the system with the "home" and "experimental" profiles.

.PARAMETER Profiles

Provide a list of profiles to install as a comma-separated list.  Default profile is "home".

.PARAMETER Platform

Specify a custom profile definition.  Default is "windows".

.PARAMETER Version

Specify a custom profile version.  Default will be looked up from the system.
		
#>


Param(
  [string[]] $Profiles = ("default"),
  [string] $Platform = "windows",
  [string] $Version = (Get-WmiObject Win32_OperatingSystem).Version
)

# self-elevate privilege if neccessary
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { 
  Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -Command `"$PSCommandPath`" -Profiles $Profiles -Platform $Platform -Version $Version" -Verb RunAs
  exit
}

Write-Host "Platform: $Platform"
Write-Host "Version: $Version"

ForEach ($profile in $Profiles) {
  if (Test-Path -Path "$PSScriptRoot\profiles\$profile") {
    Write-Host ""
    Write-Host "Profile: $profile"
    $modules = (Get-Content -Path "$PSScriptRoot\profiles\$profile")
    ForEach ($module in $modules) {
      If ($module -match "^#") {
        continue
      }
      ElseIf (Test-Path -Path ("$PScriptRoot\modules\$Platform\$Version\$module" + ".ps1")) {
        . "$PSScriptRoot\modules\$Platform\$Version\$module" + ".ps1"
      }
      ElseIf (Test-Path -Path ("$PSScriptRoot\modules\$Platform\$module" + ".ps1")) {
        . "$PSScriptRoot\modules\$Platform\$module" + ".ps1"
      }
      Else {
        Write-Host ($module+ ": skipped (not available for $Platform)")
      }
    }
  } else {
    Write-Host "ERROR: missing profile: $profile" -ForegroundColor Red
  }
}

# pause to exit
$output = @"

You've been...
 _                 _       _                   _     _ 
| |               | |     | |                 ( )   | |
| |__   ___   ___ | |_ ___| |_ _ __ __ _ _ __ |/  __| |
| '_ \ / _ \ / _ \| __/ __| __| '__/ _` | '_ \   / _` |
| |_) | (_) | (_) | |_\__ \ |_| | | (_| | |_) | | (_| |
|_.__/ \___/ \___/ \__|___/\__|_|  \__,_| .__/   \__,_|
                                        | |            
                                        |_|            

"@
Write-Host $output
Read-Host -Prompt "Press enter to exit"
