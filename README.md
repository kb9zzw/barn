# Bootstrap And Run Now!

This is a collection of bootstrap utility scripts to install and configure system software using 
various profiles.

## Quick Start

Command help:

```
# Linux/MacOS
./barn -h

# Windows
Get-Help .\barn.ps1 -Full
```

Install the `default` profile:

```
# Linux/MacOS
./barn

# Windows
.\barn.ps1
```

Install the `work` profile:

```
# Linux/MacOS
./barn -p "work"

# Windows
.\barn.ps1 -Profiles "work"
```

Install the `home` and `work` profiles:

```
# Linux/MacOS
./barn -p "home,work"

# Windows
.\barn.ps1 -Profiles "home,work"
```

## Advanced Usage

### Custom Profiles

It is possible to create your own profile by simply creating a new profile in the `profiles` directory
containing a list of supported modules. Alternatively, an existing profile can be modifed by either adding or
commenting out modules. 

If a profile references a script that doesn't exist for a given platform, it will be skipped when run.

To run a custom profile in the `profiles\custom` file:

```
# Linux/MacOS
./barn -p "custom"

# Windows
.\barn.ps1 -Profiles "custom"
```

### Adding Modules

Profiles will only run modules that match the given platform.  Modules are scripts stored in the `modules` directory.  Modules are organized by platform subdirectories. Scripts can be added to the platform directory of choice.  Linux/MacOS use shell scripts while Windows will use PowerShell scripts (denoted by `.ps1` extension).

The name of the script without the extension is the module name that can be listed in a profile.

### Adding Version-specific Modules

By default, the `barn` and `barn.ps1` scripts will look for module scripts in the base directory of the platform.  However, version-specific scripts can also be added in a nested subdirectory named with the version ID of the plaform.  For example, a version-specific `base` module for Ubuntu 21.10 can be placed in `modules/ubuntu/21.10/base` and this will override any module stored in `modules/ubuntu/base`.

### Providing a Custom Platform

By default, the scripts try to match the platform and version from operating system information. But it is also
possible to force a custom platform as follows:

```
# Linux/MacOS custom platform for "core" profile
./barn -p core -P custom

# Windows custom platform for "core" profile
.\barn.ps1 -Profiles core -Platform custom
```

This will install modules identified in the `core` module from the `modules\custom` directory.

Similarly, a custom version can also be provided, for example the following will install `core` modules from the `1.0` version of the `custom` platform:

```
# Linux/MacOS
./barn -p core -P custom -V "1.0"

# Windows
.\barn.ps1 -Profiles core -Platform custom -Version "1.0"
```




Bootstrap using the `barn` script for Linux/MacOS and `barn.ps1` for Windows.

For usage help:

```
# Linux/MacOS
./barn -h

# Windows
Get-Help .\barn.ps1 -Full
```

By default, the script will install the `home` profile, which can be viewed 
in the `profiles` subdirectory. Other or additional profiles can be installed using 
the `profiles` option with the script. For example, to install the `work` profile:

```
# Linux/MacOSX
./barn -p work

# Windows
.\barn.ps1 -Profile work
```

Multiple profiles can be specified as a comma-separated list, for example to install both the `home` and `work` profiles:

```
# Linux/MacOS
./barn -p "home,work"

# Windows
.\barn.ps1 -Profiles "home,work"
```

A custom profile can also be creatd by simply creating a new profile in the `profile` directory and adding supported modules.  Alternatively, an existing profile can be modified by either adding or commenting out modules.

By default, the script will detect the platform and look for profile modules within the corresponding `modules/$platform` directory.  If a module does not exist for a given platform, it will be skipped.

It is also possible to create a custom platform and manually setting the platform as a command-line option.  For example, if you create a collection of modules under `modules\mycustomplatform`, that platform can be specified as follows:

```
# Linux/MacOS
./barn -p "home,work" -P mycustomplatform

# Windows
.\barn.ps1 -Profiles "home,work" -Platform mycustomplatform
```

Occasionally, a platform may have multiple versions that require special treatment.  By default, the module script stored in the `module\$platform` directory will be run.  A more version-specific script can be provided by creating it in the `module\$platform\$version` subdirectory and this will take precedence over the default platform script.

Example version-specific for Ubuntu 21.10:

```
# Will prefer module scripts in modules/ubuntu/21.10 over modules/ubuntu
.\barn.ps1 -p "home,work" -P ubuntu -V "21.10"
```
