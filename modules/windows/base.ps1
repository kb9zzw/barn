# Base module

# Elevate privilege
Function install_choco() {
  # Install Chocolatey, our package manager
  Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

  choco feature enable -n useFipsCompliantChecksums
}

# Chocolatey packages
$default_packages = @(
  "gsudo",
  "git",
  "notepadplusplus",
  "putty-cac",
  "openssh",
  "openssl",
  "7zip",
  "vim"
)

if (!(Test-Path variable:base_packages)) {
  $base_packages = $default_packages
}

If (Test-Path -Path $HOME\.base_packages_installed) {
  Write-Host "base: installed"
}
Else {
  install_choco

  ForEach ($pkg in $base_packages) {
    choco install -y $pkg
  }
  Write-Output "base installed" > $HOME\.base_packages_installed
  Write-Host "base: installed"
}
