# Draw.io

$name = "drawio"
$package = "drawio"

If (choco list -le $package | Select-String -Pattern "1 packages installed" ) {
  Write-Host "${name}: installed"
}
Else {
  choco install -y $package
  Write-Host "${name}: installed"
}
