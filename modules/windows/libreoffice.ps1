# Libre Office

$name = "libreoffice"
$package = "libreoffice"

If (choco list -le $package | Select-String -Pattern "1 packages installed" ) {
  Write-Host "${name}: installed"
}
Else {
  choco install -y $package
  Write-Host "${name}: installed"
}
